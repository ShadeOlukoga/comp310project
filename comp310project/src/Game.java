import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Game extends JFrame {
    // visual parameters
    final int width  = 640; // window size
    final int height = 480;
    final int margin = 10;

    Card myCard;
    Card cpuCard;

    private Image[][] images = null; // all card pictures
    Image cardBackSide; // to display before game started
    ArrayList<Card> myDeck  = new ArrayList<Card>();
    ArrayList<Card> cpuDeck = new ArrayList<Card>();
    int myOldDeckSize = 26;
    int cpuOldDeckSize = 26;
    ArrayList<Card> stack   = new ArrayList<Card>();

    Rectangle drawable;

    Random random = new Random(); // random number generator

    // hold visual info text
    JLabel myCardCount  = new JLabel( "" );
    JLabel cpuCardCount = new JLabel( "" );
    JLabel stackCount   = new JLabel();
    JLabel statusString = new JLabel( "Welcome" );

    String allSuits = "CSHD"; // contains 4 chars for the names of the suits

    // constructor, load images and create the GUI
    public Game() {
        super( "War!" ); // create a JFrame and set the title
        try { // ImageIO.read() demands you to handle its possible errors
            cardBackSide = ImageIO.read( new File( "classic-cards/b1fv.png" ) );
        } catch (IOException e) {
            System.out.println( "Error while loading image file " );
        }

        // create the UI elements

        JPanel upperRow = new JPanel( new BorderLayout() );
        upperRow.add( myCardCount, BorderLayout.LINE_START );
        upperRow.add( stackCount, BorderLayout.CENTER );
        upperRow.add( cpuCardCount, BorderLayout.LINE_END );

        JPanel lowerRow = new JPanel( new BorderLayout() );
        lowerRow.add( statusString, BorderLayout.LINE_START );
        JButton nextButton = new JButton( "Next card" );
        lowerRow.add( nextButton, BorderLayout.LINE_END );

        // now combine the rows into one container
        JPanel container = new JPanel( new BorderLayout() );
        container.add( upperRow, BorderLayout.BEFORE_FIRST_LINE );
        container.add( lowerRow, BorderLayout.AFTER_LAST_LINE );

        // add the UI element container to the main window
        this.add( container );

        // set size of the main window
        this.setSize( width, height );

        // make the "Next card" button call its method if clicked
        nextButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent actionEvent ) {
                playNextCard();
            }
        } );

        loadCardPictures();
        initNewGame();
    }

    // card pictures must be loaded only once
    void loadCardPictures() {
        /**
         * images are in order 1 = ace of cross, 2 = ace of spaces, 3 = ace of hearts, 4 = ace of diamonds,
         * 5 = king of cross, ... 51 = two of hearts, 52 = two of diamonds, so we need to loop through them in the order
         * - for each rank from ace(14) down to 2
         *   load picture for cross, spades, hearts, diamonds
         */
        images = new Image[4][15];
        int curCard = 1;
        for (int rank = 14; rank >= 2; rank--) {
            for (int suit = 0; suit < 4; suit++) {
                try {
                    File imageFile = new File( "classic-cards/" + curCard + ".png" );
                    images[suit][rank] = ImageIO.read( imageFile );
                } catch (Exception e) {
                    System.out.println( "Error while loading image file " );
                }
                curCard++;
            }
        }
    }

    // create and return a new, sorted deck of cards
    ArrayList<Card> newDeck() {
        ArrayList<Card> deck = new ArrayList<Card>();
        for (int suitNum = 0; suitNum < 4; suitNum++) {
            for (int rank = 2; rank <= 14; rank++) {
                char suitName = allSuits.charAt( suitNum ); // the char for suite #0 is the char for "cross" - see the allSuits string
                deck.add( new Card( suitName, rank ) );
            }
        }
        return deck;
    }

    // simulate drawing from a shuffled deck by drawing from random positions of a sorted deck
    void dealDeckToAandB( ArrayList<Card> deck, ArrayList<Card> a, ArrayList<Card> b ) {
        int cardsPerPlayer = deck.size() / 2;
        Card card;
        for (int i = 0; i < cardsPerPlayer; i++) {
            card = deck.remove( random.nextInt( deck.size() ) ); // draw random card from the deck
            a.add( card ); // give it to player
            card = deck.remove( random.nextInt( deck.size() ) ); // draw another random card
            b.add( card ); // give it to the cpu
        }
    }

    // set parameters for a new game
    void initNewGame() {
        myDeck.clear();
        cpuDeck.clear();
        dealDeckToAandB( newDeck(), myDeck, cpuDeck );
        myOldDeckSize = myDeck.size();
        cpuOldDeckSize = myDeck.size();
        updateInfo();
    }

    // uncover the next two cards, compare their ranks and act accordingly
    void playNextCard() {
        if (myDeck.isEmpty() || cpuDeck.isEmpty()) {
            myCard = null;
            cpuCard = null;
            updateInfo();
            return;
        }
        myCard = myDeck.remove( 0 ); // draw the first card
        cpuCard = cpuDeck.remove( 0 );
        int result = myCard.compareTo( cpuCard );
        if (result == 0) { // if draw then update the decks before calling repaint routine
            stack.add( myCard );
            stack.add( cpuCard );
            // players with at least one card left add them to the stack
            if (!myDeck.isEmpty()) {
                stack.add( myDeck.remove( 0 ) );
                myOldDeckSize = Math.max( 0, myOldDeckSize - 2);
            }
            if (!cpuDeck.isEmpty()) {
                stack.add( myDeck.remove( 0 ) );
                cpuOldDeckSize = Math.max( 0, cpuOldDeckSize - 2);
            }
            displayMessage( "Let There Be War" );
        } else {
            // put them on the stack. when there's war, the next cards go onto the stack, too, when there's a winner
            // the stack gets cleared
            stack.add( myCard );
            stack.add( cpuCard );
            // compare results, winner gets all cards on stack
            if (result < 0) { // player rank is lower
                displayMessage( "My card" );
                cpuDeck.addAll( stack );
                stack.clear(); // don't forget to remove copies of our virtual stacks
            } else { // player rank is higher
                displayMessage( "Your card" );
                myDeck.addAll( stack );
                stack.clear();
            }
        }
        updateInfo();
    }

    // display a game status text, convenience method that saves typing the same stuff over and over
    void displayMessage( String msg ) {
        statusString.setText( msg );
    }

    // update visuals, check for a winner
    void updateInfo() {
        if (myCard == null || cpuCard == null) {
            myCardCount.setText( "" );
            cpuCardCount.setText( "" );
        } else {
            myCardCount.setText( "you: " + (myOldDeckSize) );
            cpuCardCount.setText( "cpu: " + (cpuOldDeckSize) );
        }

        repaint(); // redraw the window
        if (myDeck.isEmpty() || cpuDeck.isEmpty()) { // if anyone ran out of cards
            winningMessage();
        }
    }

    // tell who is the winner and ask if a replay is wanted
    void winningMessage() {
        String title = "";
        if (myDeck.isEmpty() && cpuDeck.isEmpty()) {
            title = "A draw. No winner.";
        } else if (myDeck.isEmpty()) {
            title = "You lose!";
        } else {
            title = "You win!";
        }

        // ask for a replay
        String[] possibleAnswers = {"Yes", "No"};
        final int YES = 0;
        int answer = JOptionPane.showOptionDialog( this, title, "Do you want to play again?", JOptionPane.YES_NO_OPTION,
                                                   JOptionPane.QUESTION_MESSAGE, null, possibleAnswers, possibleAnswers[1] );
        if (answer == YES) {
            initNewGame();
        } else {
            System.exit( 0 );
        }
    }

    @Override
    public void paint( Graphics g ) {
        if (drawable == null) { // paintable area is unknown
            int top = getInsets().top + margin + statusString.getHeight();
            int bottom = getHeight() - getInsets().bottom - statusString.getHeight() - margin;
            int left = getInsets().left + margin;
            int right = getWidth() - getInsets().right - margin;
            drawable = new Rectangle( left, top, right - left, bottom - top );
        }
        // save boundaries for convenience
        int x = (int) drawable.getX();
        int y = (int) drawable.getY();
        int w = (int) drawable.getWidth();
        int h = (int) drawable.getHeight();
        // draw UI elements
        super.paint( g );
        // draw to background buffer to minimize flickering
        BufferedImage doubleBuffer = new BufferedImage( w, h, BufferedImage.TYPE_INT_ARGB );
        Graphics db = doubleBuffer.getGraphics();
        db.setColor( Color.GREEN );
        db.fillRoundRect( 0, 0, w, h, 20, 20 );

        drawCardStack( db, myOldDeckSize, myCard, margin );
        drawCardStack( db, cpuOldDeckSize, cpuCard, w - margin - cardBackSide.getWidth( null ) );
        if (!stack.isEmpty()) {
            double centerX = drawable.getCenterX() - cardBackSide.getWidth( null ) * 0.5;
            double centerY = drawable.getCenterY() - cardBackSide.getHeight( null ) * 1.3;
            double radius = 0.75 * cardBackSide.getWidth( null );
            double deltaPhi = 2 * Math.PI / stack.size();
            double phi = 0;
            for (int i = 0; i < stack.size(); i++) {
                double xPos = centerX + Math.cos( phi )*radius;
                double yPos = centerY + Math.sin( phi )*radius;
                db.drawImage( cardBackSide, (int) xPos, (int) yPos, null );
                phi += deltaPhi;
    }
        }

        // blit background buffer to screen
        g.drawImage( doubleBuffer, x, y, null );
        myOldDeckSize = myDeck.size();
        cpuOldDeckSize = cpuDeck.size();
    }

    // draw a stack of cards at given x position
    private void drawCardStack( Graphics g, int n, Card card, int x ) {
        if (n == 0) {
            return;
        }
        int vSpace = (int) (drawable.getHeight() - cardBackSide.getHeight( null ) - 2 * margin); // space for cards
        int spacePerCard = vSpace / n;
        int y = margin + vSpace; // position of the first card
        for (int i = 0; i < n - 2; i++) { // draw covered cards
            g.drawImage( cardBackSide, x, y, null );
            y -= spacePerCard;
        }
        if (card != null) {
            Image uncovered = findImage( card );
            g.drawImage( uncovered, x, y, null );
        }
    }

    private Image findImage( Card card ) {
        // find the picture for a card with <suit> and <rank> in the images array
        int suitCode = allSuits.indexOf( card.getSuit() );
        int rankNum = card.getRank();
        return images[suitCode][rankNum];
    }

    public static void main( String[] args ) {
        Game game = new Game();
        game.setResizable( false );
        game.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        game.setVisible( true );
    }
}
